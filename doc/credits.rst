.. _credits:
.. index:: Credits

.. |br| raw:: html

  <br/>


Credits
*******

:program:`trainstation` has been developed at the `Department of
Physics <https://www.chalmers.se/en/departments/physics/Pages/default.aspx>`_
of `Chalmers University of Technology <https://www.chalmers.se/>`_ in
Gothenburg, Sweden with funding from the Knut och Alice Wallenbergs Foundation,
the Swedish Research Council, the Swedish Foundation for Strategic Research,
and the Swedish National Infrastructure for Computing.
Internally it heavily relies on :program:`scikit-learn`.
When using :program:`trainstation` in your research please therefore cite the following two papers:

* *Scikit-Learn: Machine Learning in Python* |br|
  Fabian Pedregosa *et al.* |br|
  Journal of Machine Learning Research **12**, 2825 (2011)

* *Efficient construction of linear models in materials modeling and applications to force constant expansions* |br|
  Erik Fransson, Fredrik Eriksson, and Paul Erhart |br|
  npj Computational Materials **6**, 135 (2020) |br|
  `doi: 10.1038/s41524-020-00404-5 <https://doi.org/10.1038/s41524-020-00404-5>`_
