**trainstation** — Convenient training of linear models
*******************************************************

**trainstation** is a tool that allows the efficient construction of linear models.
It provides supplementary functionality for handling data and model metrics and relies on the **scikit-learn** library for the actual training.
Many examples for the application of **trainstation** can be found in the documentation of `hiphive <https://hiphive.materialsmodeling.org/>`_ and `icet <https://icet.materialsmodeling.org/>`_.

**trainstation** has been developed  at the `Department of Physics <https://www.chalmers.se/en/departments/physics/Pages/default.aspx>`_
of `Chalmers University of Technology <https://www.chalmers.se/>`_ in Gothenburg, Sweden.
Please consult the `credits page <https://trainstation.materialsmodeling.org/credits>`_ for information on how to cite **trainstation**.
The development of **trainstation** is hosted on `gitlab <https://gitlab.com/materials-modeling/trainstation>`_.
Bugs should be submitted via the `gitlab issue tracker <https://gitlab.com/materials-modeling/trainstation/issues>`_.

Installation
------------

**trainstation** can be installed via ``pip``::

    pip3 install trainstation

If you want to get the absolutely latest version you can install from the repo::

    pip3 install -e git+https://gitlab.com/materials-modeling/trainstation#egg=trainstation
